﻿using System;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageEditor.Tools;
using System.Drawing.Imaging;

namespace ImageEditor
{
    public partial class DrawControl : System.Windows.Forms.PictureBox
    {
        #region Members

        private DrawToolType m_activeTool;

        private GraphicsList m_graphicsList;
        private Tool[] m_tools;

        private GraphicsProperties m_property;

        #endregion


        #region Constructor

        public DrawControl()
        {
            InitializeComponent();
        }

        public void Initialize()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint |
                ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);


            m_activeTool = DrawToolType.Pointer;

            
            m_graphicsList = new GraphicsList();

            m_tools = new Tool[(int)DrawToolType.NumberOfDrawTools];
            m_tools[(int)DrawToolType.Pointer] = new ToolPointer();
            m_tools[(int)DrawToolType.Rectangle] = new ToolRectangle();
            m_tools[(int)DrawToolType.Line] = new ToolLine();
            m_tools[(int)DrawToolType.Circle] = new ToolCirle();
            m_tools[(int)DrawToolType.Pencil] = new ToolPencile();
        }


        #endregion

        #region Properties

        public DrawToolType ActiveTool
        {
            get
            {
                return m_activeTool;
            }
            set
            {
                m_activeTool = value;
                
            }

        }

        public GraphicsList GraphicsList
        {
            get
            {
                return m_graphicsList;
            }

            set
            {
                m_graphicsList = value;
            }
        }

        public GraphicsProperties GraphicsProperty
        {
            get
            {
                return m_property;
            }

            set
            {
                m_property = value;
                //m_graphicsList.ApplyProperties(value);
            }
        }


        #endregion

        
        #region Events

        private void DrawArea_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                m_tools[(int)m_activeTool].OnMouseUp(this, e);
        }

        private void DrawArea_Paint(object sender, PaintEventArgs e)
        {
            if (m_graphicsList != null)
            {

                m_graphicsList.Draw(e.Graphics);

            }

        }

        private void DrawArea_MouseMove(object sender, MouseEventArgs e)
        {
            //необходимая мера так как он определяет что две кнопки нажаты одновременно
            //if ((int)e.Button == (int)MouseButtons.Left+(int)MouseButtons.XButton1)
            if (e.Button == MouseButtons.Left)
                    m_tools[(int)m_activeTool].OnMouseMove(this, e);
            
        }

        private void DrawArea_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                m_tools[(int)m_activeTool].OnMouseDown(this, e);
        }

        #endregion


        #region Other function

        public bool SaveImage(String filePath,String mapType)
        {

            m_graphicsList.Draw(Graphics.FromImage(this.Image));

            Bitmap myBitmap = (Bitmap)this.Image;
            ImageCodecInfo myImageCodecInfo = GetEncoderInfo(mapType);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 100L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            try
            {
                myBitmap.Save(filePath, myImageCodecInfo, myEncoderParameters);
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        public void OperationImage(ImageProccessing imageProcess)
        {
            m_graphicsList.Draw(Graphics.FromImage(this.Image));
            m_graphicsList.Clear();

            ProcessingForm processForm = new ProcessingForm((Bitmap)this.Image);
            processForm.ActiveImageOperation = imageProcess;
            processForm.ShowDialog();

            this.Image = processForm.BitmapSource;
        }

        private ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }



        #endregion

    }
}
