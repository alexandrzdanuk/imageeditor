﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEditor
{
    public partial class ProcessingForm : Form
    {
        
        private ImageProccessing m_activeImageOperation;
        private Bitmap m_bitmapSource;
        private MyThread m_myTread;


        public ProcessingForm()
        {
            m_bitmapSource = null;
            m_activeImageOperation = ImageProccessing.None;
            InitializeComponent();
        }

        public ProcessingForm(Bitmap bitmap)
        {
            m_bitmapSource = bitmap;
            m_activeImageOperation = ImageProccessing.None;
            InitializeComponent();
            timer.Stop();
        }



        public ImageProccessing ActiveImageOperation
        {
            get
            {
                return m_activeImageOperation;
            }

            set
            {
                m_activeImageOperation = value;
            }
        }

        public Bitmap BitmapSource
        {
            get
            {
                return m_bitmapSource;
            }

        }

        private void ProcessingForm_Shown(object sender, EventArgs e)
        {
            if (BitmapSource == null && m_activeImageOperation == ImageProccessing.None)
                return;
            m_myTread = new MyThread(BitmapSource, m_activeImageOperation, "General thread");
            timer.Start();

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            progressBar.Value = m_myTread.StatePercentThread;
            lableProgressBar.Text = m_myTread.StatePercentThread + " %";
            if (m_myTread.GetState == ThreadState.Stopped && progressBar.Value == 100) Close();
        }

        
    }
}
