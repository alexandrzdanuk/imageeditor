﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImageEditor
{
    class MyThread
    {
        private Thread m_thread;

        private ImageProccessing m_activeProcess;
        private Bitmap m_bitmapSource;

        private int m_statePercentThread;

        public int StatePercentThread
        {
            get
            {
                return m_statePercentThread;
            }
            
        }

        public ThreadState GetState
        {
            get
            {
                return m_thread.ThreadState;
            }
        }


        public MyThread()
        {

        }

        
        public MyThread(Bitmap source,ImageProccessing activeProcess,String nameThread)
        {
            m_activeProcess = activeProcess;
            m_bitmapSource = source;

            switch (m_activeProcess)
            {
                case ImageProccessing.None:
                    break;
                case ImageProccessing.Inverse:
                    m_thread = new Thread(this.Inverse);
                    m_thread.Name = nameThread;
                    m_thread.Start();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(m_activeProcess.ToString());
            }
        }


        public void Inverse(Object bitmap)
        {
            m_statePercentThread = 0;
            int Index = 0;
            
            for (int x = 0; x <= m_bitmapSource.Width - 1; x++)
            {
                for (int y = 0; y <= m_bitmapSource.Height - 1; y += 1)
                {
                    Color oldColor = m_bitmapSource.GetPixel(x, y);
                    Color newColor;
                    newColor = Color.FromArgb(oldColor.A, 255 - oldColor.R, 255 - oldColor.G, 255 - oldColor.B);
                    m_bitmapSource.SetPixel(x, y, newColor);
                    Index++;
                    
                    m_statePercentThread = Convert.ToInt32((Index / (m_bitmapSource.Height*m_bitmapSource.Width*1.0)) * 100);
                    Thread.Sleep(1);
                }
            }

        }

    }
}
