﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageEditor
{
    public class GraphicsProperties
    {
        private Color m_colorPen;
        private int m_penWidth;
        private Color m_colorBrush;

        private bool m_colorPenDefined;
        private bool m_penWidthDefined;
        private bool m_colorBrushDefined;


        public GraphicsProperties()
        {
            m_colorPen = Color.Black;
            m_penWidth = 1;
            m_colorPenDefined = false;
            m_penWidthDefined = false;
        }

        public GraphicsProperties(Color colorPen)
        {
            m_colorPen = colorPen;
            m_penWidth = 1;
            m_colorPenDefined = true;
            m_penWidthDefined = false;
        }

        public GraphicsProperties(Color colorPen, Color colorBrush)
        {
            m_colorPen = colorPen;
            m_colorBrush = colorBrush;
            m_penWidth = 1;
            m_colorPenDefined = true;
            m_penWidthDefined = false;
            m_colorBrushDefined = true;
        }


        public GraphicsProperties(Color colorPen, Color colorBrush, int penWidth)
        {
            m_colorPen = colorPen;
            m_colorBrush = colorBrush;
            m_penWidth = penWidth;
            m_colorPenDefined = true;
            m_penWidthDefined = true;
            m_colorBrushDefined = true;
        }

        public Color ColorPen
        {
            get
            {
                return m_colorPen;
            }
            set
            {
                m_colorPen = value;
            }
        }

        public Color ColorBrush
        {
            get
            {
                return m_colorBrush;
            }
            set
            {
                m_colorBrush = value;
            }
        }

        public int PenWidth
        {
            get
            {
                return m_penWidth;
            }
            set
            {
                m_penWidth = value;
            }
        }

        public bool ColorPenDefined
        {
            get
            {
                return m_colorPenDefined;
            }
            set
            {
                m_colorPenDefined = value;
            }
        }

        public bool ColorBrushDefined
        {
            get
            {
                return m_colorBrushDefined;
            }
            set
            {
                m_colorBrushDefined = value;
            }
        }

        public bool PenWidthDefined
        {
            get
            {
                return m_penWidthDefined;
            }
            set
            {
                m_penWidthDefined = value;
            }
        }
    }
}
