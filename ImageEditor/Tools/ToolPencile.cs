﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEditor.Tools
{
    public class ToolPencile : Tool   
    {
        private int m_lastX;
        private int m_lastY;
        private GraphicsPenсil m_newPencil;
        private const int minDistance = 15 * 15;


        public ToolPencile()
        {
        }

        public override void OnMouseDown(DrawControl drawControl, MouseEventArgs e)
        {
            m_newPencil = new GraphicsPenсil(e.X, e.Y, e.X + 1, e.Y + 1,drawControl.GraphicsProperty);
            AddNewObject(drawControl, m_newPencil);
            m_lastX = e.X;
            m_lastY = e.Y;
        }

        public override void OnMouseMove(DrawControl drawControl, MouseEventArgs e)
        {
            if (m_newPencil == null)
                return;                 

            Point point = new Point(e.X, e.Y);
            m_newPencil.AddPoint(point);
            m_lastX = e.X;
            m_lastY = e.Y;
            
            drawControl.Refresh();
        }

        public override void OnMouseUp(DrawControl drawControl, MouseEventArgs e)
        {
            m_newPencil = null;

            base.OnMouseUp(drawControl, e);
        }

    }
}
