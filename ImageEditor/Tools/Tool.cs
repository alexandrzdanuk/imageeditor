﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ImageEditor
{
    public abstract class Tool
    {
        private Cursor cursor;

        protected Cursor Cursor
        {
            get
            {
                return cursor;
            }
            set
            {
                cursor = value;
            }
        }

        public virtual void OnMouseDown(DrawControl drawControl, MouseEventArgs e)
        {

        }


        public virtual void OnMouseMove(DrawControl drawControl, MouseEventArgs e)
        {
        }


        public virtual void OnMouseUp(DrawControl drawControl, MouseEventArgs e)
        {
            
        }

        protected void AddNewObject(DrawControl drawControl, GraphicsBaseObject o)
        {
            o.Selected = true;
            drawControl.GraphicsList.Add(o);

            drawControl.Capture = true;
            drawControl.Refresh();
            
        }
    }
}
