﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEditor.Tools
{
    class ToolPointer : ImageEditor.Tool
    {
        private enum SelectionMode
        {
            None,
            NetSelection,   
            Move,           
            Size            
        }

        private SelectionMode selectMode = SelectionMode.None;

        
        private DrawControl resizedObject;
        private int resizedObjectHandle;

        private Point lastPoint = new Point(0, 0);
        private Point startPoint = new Point(0, 0);

        public ToolPointer()
        {
        }

        public override void OnMouseDown(DrawControl drawControl, MouseEventArgs e)
        {
            drawControl.Refresh();
        }


        public override void OnMouseMove(DrawControl drawControl, MouseEventArgs e)
        {
            drawControl.Refresh();
        }


        public override void OnMouseUp(DrawControl drawControl, MouseEventArgs e)
        {
            
            drawControl.Capture = false;
            drawControl.Refresh();
        }


    }
}
