﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEditor.Tools
{
    class ToolLine : Tool
    {
        private GraphicsLine m_newLine;

        public ToolLine()
        {
            
        }

        public override void OnMouseDown(DrawControl drawControl, MouseEventArgs e)
        {
            m_newLine = new GraphicsLine(e.X, e.Y, e.X + 1, e.Y + 1,drawControl.GraphicsProperty);
            AddNewObject(drawControl, m_newLine);
        }

        public override void OnMouseMove(DrawControl drawControl, MouseEventArgs e)
        {
            drawControl.Cursor = Cursor;
            Point point = new Point(e.X, e.Y);
            m_newLine.MoveHandleTo(point, 2);
            drawControl.Refresh();
        }

        public override void OnMouseUp(DrawControl drawControl, MouseEventArgs e)
        {
            m_newLine = null;
        }

    }
}
