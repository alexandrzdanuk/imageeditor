﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEditor.Tools
{
    class ToolCirle : ToolRectangle
    {
        private GraphicsCircle m_newCircle;

        public ToolCirle()
        {
           
        }

        public override void OnMouseDown(DrawControl drawControl, MouseEventArgs e)
        {
            m_newCircle = new GraphicsCircle(e.X, e.Y, 1, 1,drawControl.GraphicsProperty);
            
            AddNewObject(drawControl, m_newCircle);
        }

        public override void OnMouseMove(DrawControl drawControl, MouseEventArgs e)
        {
            drawControl.Cursor = Cursor;
            Point point = new Point(e.X, e.Y);
            m_newCircle.MoveHandleTo(point, 5);
            drawControl.Refresh();
        }

    }
}
