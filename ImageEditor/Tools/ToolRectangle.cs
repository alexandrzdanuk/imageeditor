﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEditor.Tools
{
    public class ToolRectangle : Tool   
    {
        private GraphicsRectangle m_newRectangle;

        public ToolRectangle()
        {
        }

        public override void OnMouseDown(DrawControl drawControl, MouseEventArgs e)
        {
            m_newRectangle = new GraphicsRectangle(e.X, e.Y, 1, 1,drawControl.GraphicsProperty);
            AddNewObject(drawControl, m_newRectangle);
        }

        public override void OnMouseMove(DrawControl drawControl, MouseEventArgs e)
        {
            drawControl.Cursor = Cursor;
            Point point = new Point(e.X, e.Y);
            m_newRectangle.MoveHandleTo(point, 5);
            drawControl.Refresh();
        }

    }
}
