﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageEditor
{
    public enum ImageProccessing
    {
        None = 0,
        Inverse = 1
    };


    public enum DrawToolType
    {
        Pointer,
        Pencil,
        Rectangle,
        Circle,
        Line,
        NumberOfDrawTools
    };
}
