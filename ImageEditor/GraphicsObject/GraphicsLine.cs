﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageEditor
{
    class GraphicsLine : GraphicsBaseObject
    {
        private Point m_startPoint;
        private Point m_endPoint;

  //      public GraphicsLine() : this(0, 0, 1, 0, new GraphicsProperties(Color.Black,Color.Black,1))
		//{
  //      }

        public GraphicsLine(int x1, int y1, int x2, int y2,GraphicsProperties property) : base()
        {
            m_startPoint.X = x1;
            m_startPoint.Y = y1;
            m_endPoint.X = x2;
            m_endPoint.Y = y2;

            if (property.ColorPenDefined)
                ColorStyle = property.ColorPen;
            if (property.PenWidthDefined)
                PenWidthStyle = property.PenWidth;

            Initialize();
        }

        public override void Draw(Graphics g)
        {
            g.SmoothingMode = SmoothingMode.AntiAlias;

            Pen pen = new Pen(ColorStyle, PenWidthStyle);

            g.DrawLine(pen, m_startPoint.X, m_startPoint.Y, m_endPoint.X, m_endPoint.Y);

            pen.Dispose();
        }

        public override void MoveHandleTo(Point point, int handleNumber)
        {
            if (handleNumber == 1)
                m_startPoint = point;
            else
                m_endPoint = point;

        }
        
    }
}
