﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;

namespace ImageEditor
{
    class GraphicsPenсil : GraphicsBaseObject
    {
        #region Class members

        private List<Point> m_pointList;

        #endregion

        #region Constructor

        public GraphicsPenсil()
        {
            m_objectId = this.GetHashCode();
        }

        public GraphicsPenсil(int x1, int y1, int x2, int y2,GraphicsProperties property)
        {
            m_pointList = new List<Point>();
            m_pointList.Add(new Point(x1, y1));
            m_pointList.Add(new Point(x2, y2));

            if (property.ColorPenDefined)
                m_color = property.ColorPen;
            if (property.PenWidthDefined)
                m_penWidth = property.PenWidth;

        }

        #endregion


        #region Function

        public override void Draw(Graphics graphics)
        {
            int x1 = 0, y1 = 0;     
            int x2, y2;             

            graphics.SmoothingMode = SmoothingMode.AntiAlias;

            Pen pen = new Pen(ColorStyle, PenWidthStyle);

            x1 = m_pointList[0].X;
            y1 = m_pointList[0].Y;
        
            foreach (Point point in m_pointList)
            {
                x2 = point.X;
                y2 = point.Y;

                if (x1 == x2 && y1 == y2) continue;

                graphics.DrawLine(pen, x1, y1, x2, y2);

                x1 = x2;
                y1 = y2;
            }

            pen.Dispose();

            base.Draw(graphics);
        }


        public void AddPoint(Point point)
        {
            m_pointList.Add(point);
        }


        #endregion
    }
}
