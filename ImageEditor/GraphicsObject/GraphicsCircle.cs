﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageEditor
{
    class GraphicsCircle : GraphicsRectangle
    {
        public GraphicsCircle() : this(0, 0, 1, 1, new GraphicsProperties(Color.Black, Color.Black, 1))
		{
        }

        public GraphicsCircle(int x, int y, int width, int height,GraphicsProperties property) : base()
        {
            Rectangle = new Rectangle(x, y, width, height);

            if (property.ColorPenDefined)
                m_color = property.ColorPen;
            if (property.PenWidthDefined)
                m_penWidth = property.PenWidth;
            if (property.ColorBrushDefined)
            {
                m_colorBrush = property.ColorBrush;
                m_colorBrushDefined = true;
            }
            Initialize();
        }

        public override void Draw(Graphics g)
        {
            Pen pen = new Pen(ColorStyle, PenWidthStyle);

            g.DrawEllipse(pen, GraphicsRectangle.GetNormalizedRectangle(Rectangle));

            if (m_colorBrushDefined)
                g.FillEllipse(new SolidBrush(m_colorBrush), GraphicsRectangle.GetNormalizedRectangle(Rectangle));

            pen.Dispose();
        }


    }
}
