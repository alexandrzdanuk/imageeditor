﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.Serialization;

namespace ImageEditor
{
    public abstract class GraphicsBaseObject
    {
        #region Class members

        
        protected Point m_pointStart;
        protected bool m_selected;
        protected Graphics m_graphics;

        protected Color m_color;
        protected int m_penWidth;

        protected static Color lastUsedColor = Color.Black;
        protected static int lastUsedPenWidth = 1;

        protected int m_objectId;

        #endregion

        #region Constructor

        protected GraphicsBaseObject()
        {
            m_objectId = this.GetHashCode();
        }

        
        #endregion

        #region Properties

        public Color ColorStyle
        {
            get
            {
                return m_color;
            }
            set
            {
                m_color = value;
                RefreshDrawing();
            }
        }

        public Point StartPoint
        {
            get
            {
                return m_pointStart;
            }
            set
            {
                m_pointStart = value;
                RefreshDrawing();
            }
        }

        public bool Selected
        {
            get
            {
                return m_selected;
            }
            set
            {
                m_selected = value;
                RefreshDrawing();
            }
        }


        public Graphics GraphicsElement
        {
            get
            {
                return m_graphics;
            }
            set
            {
                m_graphics = value;
            }
        }

        public int ObjectId
        {
            get
            {
                return m_objectId;
            }
        }

        public int PenWidthStyle
        {
            get
            {
                return m_penWidth;
            }
            set
            {
                m_penWidth = value;
                RefreshDrawing();
            }
        }

        public static Color LastUsedColor
        {
            get
            {
                return lastUsedColor;
            }

            set
            {
                lastUsedColor = value;
            }
        }

        public static int LastUsedPenWidth
        {
            get
            {
                return lastUsedPenWidth;
            }

            set
            {
                lastUsedPenWidth = value;
            }
        }



        #endregion


        #region Virtual Methods

        public virtual void RefreshDrawing()
        {
            if (m_selected && m_graphics is Graphics)
            {
                Draw(m_graphics);
            }
        }

        public virtual void Draw(Graphics g)
        {

        }

        public virtual void MoveHandleTo(Point point, int handleNumber)
        {
        }

        protected void Initialize()
        {
        }

        #endregion
    }
}
