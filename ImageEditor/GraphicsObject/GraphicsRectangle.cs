﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections;
using System.Windows.Forms;

namespace ImageEditor
{
    class GraphicsRectangle : GraphicsBaseObject
    {
        private Rectangle m_rectangle;
        protected Color m_colorBrush;
        protected bool m_colorBrushDefined;

        protected Rectangle Rectangle
        {
            get
            {
                return m_rectangle;
            }
            set
            {
                m_rectangle = value;
            }
        }

        public GraphicsRectangle()
        {
            SetRectangle(0, 0, 1, 1);
            Initialize();
        }

        private void Initialize()
        {
        }

        public GraphicsRectangle(int x, int y, int width, int height, GraphicsProperties property)
        {
            m_rectangle.X = x;
            m_rectangle.Y = y;
            m_rectangle.Width = width;
            m_rectangle.Height = height;

            if (property.ColorPenDefined)
                m_color = property.ColorPen;
            if (property.PenWidthDefined)
                m_penWidth = property.PenWidth;
            if (property.ColorBrushDefined)
            {
                m_colorBrush = property.ColorBrush;
                m_colorBrushDefined = true;
            }


            Initialize();
        }


       public override void Draw(Graphics g)
        {
            Pen pen = new Pen(ColorStyle, PenWidthStyle);

            g.DrawRectangle(pen, GraphicsRectangle.GetNormalizedRectangle(Rectangle));

            if (m_colorBrushDefined)
                g.FillRectangle(new SolidBrush(m_colorBrush), GraphicsRectangle.GetNormalizedRectangle(Rectangle));

            pen.Dispose();
        }

        protected void SetRectangle(int x, int y, int width, int height)
        {
            m_rectangle.X = x;
            m_rectangle.Y = y;
            m_rectangle.Width = width;
            m_rectangle.Height = height;
        }

        
        public  void Move(int deltaX, int deltaY)
        {
            m_rectangle.X += deltaX;
            m_rectangle.Y += deltaY;
        }

        public void Normalize()
        {
            m_rectangle = GraphicsRectangle.GetNormalizedRectangle(m_rectangle);
        }

        public override void MoveHandleTo(Point point, int handleNumber)
        {
            int left = Rectangle.Left;
            int top = Rectangle.Top;
            int right = Rectangle.Right;
            int bottom = Rectangle.Bottom;

            switch (handleNumber)
            {
                case 1:
                    left = point.X;
                    top = point.Y;
                    break;
                case 2:
                    top = point.Y;
                    break;
                case 3:
                    right = point.X;
                    top = point.Y;
                    break;
                case 4:
                    right = point.X;
                    break;
                case 5:
                    right = point.X;
                    bottom = point.Y;
                    break;
                case 6:
                    bottom = point.Y;
                    break;
                case 7:
                    left = point.X;
                    bottom = point.Y;
                    break;
                case 8:
                    left = point.X;
                    break;
            }

            SetRectangle(left, top, right - left, bottom - top);
        }



        #region Helper Functions

        public static Rectangle GetNormalizedRectangle(int x1, int y1, int x2, int y2)
        {
            if (x2 < x1)
            {
                int tmp = x2;
                x2 = x1;
                x1 = tmp;
            }

            if (y2 < y1)
            {
                int tmp = y2;
                y2 = y1;
                y1 = tmp;
            }

            return new Rectangle(x1, y1, x2 - x1, y2 - y1);
        }

        public static Rectangle GetNormalizedRectangle(Point p1, Point p2)
        {
            return GetNormalizedRectangle(p1.X, p1.Y, p2.X, p2.Y);
        }

        public static Rectangle GetNormalizedRectangle(Rectangle r)
        {
            return GetNormalizedRectangle(r.X, r.Y, r.X + r.Width, r.Y + r.Height);
        }

        #endregion

    }
}
