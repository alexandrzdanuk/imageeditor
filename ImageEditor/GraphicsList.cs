﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEditor
{
    
    public class GraphicsList 
    {
        private ArrayList m_graphicsList;

        private const string m_entryCount = "Count";
        private const string m_entryType = "Type";
        private GraphicsProperties m_properties;

        public GraphicsList()
        {
            m_graphicsList = new ArrayList();
        }

        

        public void Draw(Graphics g)
        {
            int n = m_graphicsList.Count;
            GraphicsBaseObject o;

            // Enumerate list in reverse order
            // to get first object on the top
            for (int i = n - 1; i >= 0; i--)
            {
                o = (GraphicsBaseObject)m_graphicsList[i];

                o.Draw(g);
            }
        }

        
        public bool Clear()
        {
            bool result = (m_graphicsList.Count > 0);
            m_graphicsList.Clear();
            return result;
        }

       
        public int Count
        {
            get
            {
                return m_graphicsList.Count;
            }
        }

        public GraphicsBaseObject this[int index]
        {
            get
            {
                if (index < 0 || index >= m_graphicsList.Count)
                    return null;

                return ((GraphicsBaseObject)m_graphicsList[index]);
            }
        }

       
        public int SelectionCount
        {
            get
            {
                int n = 0;

                foreach (GraphicsBaseObject o in m_graphicsList)
                {
                    if (o.Selected)
                        n++;
                }

                return n;
            }
        }

        public GraphicsBaseObject GetSelectedObject(int index)
        {
            int n = -1;

            foreach (GraphicsBaseObject o in m_graphicsList)
            {
                if (o.Selected)
                {
                    n++;

                    if (n == index)
                        return o;
                }
            }

            return null;
        }

        public void Add(GraphicsBaseObject obj)
        {
            m_graphicsList.Insert(0, obj);
        }



        public GraphicsProperties GetProperties()
        {
            m_properties = new GraphicsProperties();

            int n = SelectionCount;

            if (n < 1)
                return m_properties;

            GraphicsBaseObject o = GetSelectedObject(0);

            int firstColor = o.ColorStyle.ToArgb();
            int firstPenWidth = o.PenWidthStyle;

            bool allColorsAreEqual = true;
            bool allWidthAreEqual = true;

            for (int i = 1; i < n; i++)
            {
                if (GetSelectedObject(i).ColorStyle.ToArgb() != firstColor)
                    allColorsAreEqual = false;

                if (GetSelectedObject(i).PenWidthStyle != firstPenWidth)
                    allWidthAreEqual = false;
            }

            if (allColorsAreEqual)
            {
                m_properties.ColorPenDefined = true;
                m_properties.ColorPen = Color.FromArgb(firstColor);
            }

            if (allWidthAreEqual)
            {
                m_properties.PenWidthDefined = true;
                m_properties.PenWidth = firstPenWidth;
            }

            return m_properties;
        }


        public void ApplyProperties(GraphicsProperties properties)
        {
            foreach (GraphicsBaseObject o in m_graphicsList)
            {
                if (o.Selected)
                {
                    if (properties.ColorPenDefined)
                    {
                        o.ColorStyle = properties.ColorPen;
                        GraphicsBaseObject.LastUsedColor = properties.ColorPen;
                    }

                    if (properties.PenWidthDefined)
                    {
                        o.PenWidthStyle = properties.PenWidth;
                        GraphicsBaseObject.LastUsedPenWidth = properties.PenWidth;
                    }
                }
            }
        }

        
    }
}
