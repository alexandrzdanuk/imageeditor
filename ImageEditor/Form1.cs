﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEditor
{
    public partial class GeneralForm : Form
    {
        
        public GeneralForm()
        {
            InitializeComponent();
            m_drawControl.Initialize();
        }

        private void GeneralForm_Load(object sender, EventArgs e)
        {

            ResizeDrawControl();
            colorDialogBrush.Color = Properties.Settings.Default.colorBrush;
            colorDialogPen.Color = Properties.Settings.Default.colorPen;

            toolStripButtonColorPen.BackColor = Properties.Settings.Default.colorPen;
            toolStripButtonColorBrush.BackColor = Properties.Settings.Default.colorBrush;
            toolStripComboBoxPenStyle.SelectedItem = toolStripComboBoxPenStyle.Items[toolStripComboBoxPenStyle.FindStringExact(Properties.Settings.Default.penWidth.ToString())];

            m_drawControl.GraphicsProperty = new GraphicsProperties(colorDialogPen.Color, colorDialogBrush.Color, Convert.ToInt32(toolStripComboBoxPenStyle.SelectedItem));

        }
        
        private void GeneralForm_Resize(object sender, EventArgs e)
        {
            ResizeDrawControl();
        }

        private void ResizeDrawControl()
        {
            Rectangle rect = this.ClientRectangle;

            m_drawControl.Left = rect.Left;
            m_drawControl.Top = rect.Top + toolStrip.Height + menuStrip.Height;
            m_drawControl.Width = rect.Width;
            m_drawControl.Height = rect.Height - toolStrip.Height - menuStrip.Height;
        }

        private void toolStripButtonNew_Click(object sender, EventArgs e)
        {
            Bitmap flag = new Bitmap(this.ClientRectangle.Width,this.ClientRectangle.Height);
            Graphics flagGraphics = Graphics.FromImage(flag);
            SolidBrush brush = new SolidBrush(Color.FromArgb(255, 255, 255));
            flagGraphics.FillRectangle(brush, this.ClientRectangle);
            m_drawControl.Image = flag;  
        }

        private void toolStripButtonOpen_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                OpenImageToPictireBox(openFileDialog.FileName);
            }
        }

        private void toolStripButtonSave_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                var extension = Path.GetExtension(saveFileDialog.FileName);

                switch (extension.ToLower())
                {
                    case ".jpeg":
                        m_drawControl.SaveImage(saveFileDialog.FileName, "image/jpeg");
                        break;
                    case ".png":
                        m_drawControl.SaveImage(saveFileDialog.FileName, "image/png");
                        break;
                    case ".jpg":
                        m_drawControl.SaveImage(saveFileDialog.FileName, "image/jpeg");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(extension);
                }

                m_drawControl.GraphicsList.Clear();
                OpenImageToPictireBox(saveFileDialog.FileName);

            }
        }




        private void toolStripButtonPencil_Click(object sender, EventArgs e)
        {
            m_drawControl.ActiveTool = DrawToolType.Pencil;
        }

        private void toolStripButtonPointer_Click(object sender, EventArgs e)
        {
            m_drawControl.ActiveTool = DrawToolType.Pointer;
        }

        private void toolStripButtonLine_Click(object sender, EventArgs e)
        {
            m_drawControl.ActiveTool = DrawToolType.Line;

        }

        private void toolStripButtonRectangle_Click(object sender, EventArgs e)
        {
            m_drawControl.ActiveTool = DrawToolType.Rectangle;
        }

        private void toolStripButtonCircle_Click(object sender, EventArgs e)
        {
            m_drawControl.ActiveTool = DrawToolType.Circle;
        }



        private void toolStripButtonColorPen_Click(object sender, EventArgs e)
        {
            if (colorDialogPen.ShowDialog() == DialogResult.OK)
            {
                toolStripButtonColorPen.BackColor = colorDialogPen.Color;
                m_drawControl.GraphicsProperty = new GraphicsProperties(colorDialogPen.Color,colorDialogBrush.Color,Convert.ToInt32(toolStripComboBoxPenStyle.SelectedItem));
            }
        }

        private void toolStripButtonColorBrush_Click(object sender, EventArgs e)
        {
            if (colorDialogBrush.ShowDialog() == DialogResult.OK)
            {
                toolStripButtonColorBrush.BackColor = colorDialogBrush.Color;
                m_drawControl.GraphicsProperty = new GraphicsProperties(colorDialogPen.Color, colorDialogBrush.Color, Convert.ToInt32(toolStripComboBoxPenStyle.SelectedItem));
            }
        }

        private void toolStripComboBoxPenStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            m_drawControl.GraphicsProperty = new GraphicsProperties(colorDialogPen.Color, colorDialogBrush.Color, Convert.ToInt32(toolStripComboBoxPenStyle.SelectedItem));
        }

        public void OpenImageToPictireBox(String filePath)
        {
            Bitmap image = new Bitmap(Image.FromFile(filePath));
            Bitmap pageImage;
            switch (image.PixelFormat)
            {
                case PixelFormat.DontCare:
                case PixelFormat.Format1bppIndexed:
                case PixelFormat.Format4bppIndexed:
                case PixelFormat.Format8bppIndexed:
                case PixelFormat.Format16bppGrayScale:
                case PixelFormat.Format16bppArgb1555:
                case PixelFormat.Indexed:
                    pageImage = new Bitmap(image.Width, image.Height, PixelFormat.Format16bppRgb565);
                    pageImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                    break;
                default:
                    pageImage = image;
                    break;
            }

            m_drawControl.Image = pageImage;
        }

        private void CloseAppToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void InversToolStripMenuItem_Click(object sender, EventArgs e)
        {
            m_drawControl.OperationImage(ImageProccessing.Inverse);
        }

        private void GeneralForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.penWidth = Convert.ToInt32(toolStripComboBoxPenStyle.SelectedItem.ToString());
            Properties.Settings.Default.colorBrush = colorDialogBrush.Color;
            Properties.Settings.Default.colorPen = colorDialogPen.Color;
            Properties.Settings.Default.Save();

        }
    }
}
